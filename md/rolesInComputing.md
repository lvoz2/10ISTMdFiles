# Roles in Computing

<h2 style="display:none" id="top">(top)</h2>

![](https://techcrunch.com/wp-content/uploads/2015/04/codecode.jpg?w=1920&crop=1)

## Introduction

There are many roles within computing, such as programmers, who write the code that gets executed on devices created by electronics engineers.

## Software based

Some of the software based computing jobs include, but are not limited to:

- Application Analyst
- Business Analyst
- Data entry clerk
- Database Administrator
- Data Analyst
- Data designer
- Network Analyst
- Network Administrator
- Programmmer
- Project Manager
- Rapid Prototyper
- Scrum Master
- Security Engineer
- Software Analyst
- Software Architect
- Software Designer
- Software Engineer
- Software Project Manager
- Software Quality Analyst
- Software Test Engineer
- Solution Architect
- Support Technician
- System Administrator
- Systems Analyst
- Systems Architect
- User Experience Designer
- User Interaction Designer
- User Researcher
- Video Game Developer
- Visual Designer
- Web Developer
- Website Administrator

Some of these jobs merely work with software and data, while others create the software that eventually gets used to create the apps that make society work.

## Hardware based

- Computer Operator
- Computer Repair Technician
- Computer Analyst
- Hardware Engineer
- IT technician
- IT assistant
- IT consultant
- Electronics Engineer
- PCB designer
- Electronics Technician
- Semiconductor Development Technician
- IC Chip Layout Designer

## Bibliography

- Wikimedia Foundation (16 June 2017) [Occupations in electrical/electronics engineering](https://en.wikipedia.org/wiki/Occupations_in_electrical/electronics_engineering), Wikipedia, accessed 2 April 2023
- Wikimedia Foundation (11 March 2023) [List of computer occupations](https://en.wikipedia.org/wiki/List_of_computer_occupations), Wikipedia, accessed 2 April 2023