# Ethical Implications of Hacking

<h2 style="display:none" id="top">(top)</h2>

![Picture of a black-hat hacker looking up from their screen and pointing at the audience](https://images.theconversation.com/files/197295/original/file-20171201-10155-re4sju.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=7680&fit=clip)

## Introduction

Hackers around the world can be split into two groups:

- White Hat, which are also security researchers and who will hack into systems to find vulnerablities and notify the company. These hackers are not malicious
- Black Hat, which are the stereotypical hackers, who will hack into systems for the data they possess, so they can sell it for profit or use it to get details that can be combined to steal identities or money from bank accounts. These are the kind that have malicious intent

For the white hat hacker, ethics plays a big role in how they conduct themselves, but on the other hand, black hat hackers are normally motivated financially to hack into systems, and will do so without the consent of the owner of the system

## White Hat Hackers

White Hat hackers, also known as ethical hackers, are a class of hackers who will hack into systems with the consent of the owner of the system, with a contract descibing what the hacker is allowed to do, the objective of the hack, and whether they are allowed to patch holes found in the system. Many of these hackers are employed as computer security experts. When a white hat hacker hacks, they will almost universally make notes and document everything they do, so that others can understand thought processes and to check whether the contract is being upheld. The report also provides a way for others to fix vulnerablities found, in the case where the hacker themselves are not allowed to tamper with the system. When a white hat hacker hacks into a system illegally with the intent of helping and fixing security flaws, they are reffered to as a 'grey hat hacker'.

## Black Hat Hackers

Black Hat hackers are the other kind of hacker, and are normally responsible for installing backdoors or malware onto devices, or when information is stolen from a system. They are almost always financially motivated, and can sometimes be indirectly or directly politically motivated, such as is the case when a a hacker group is sponsored by a country or state to attack a certain target. Not all cyberattacks are caused by black hat hackers, but the vast majority are, with the larger cyberattacks often happening because a state has sponsored an attack. However, not all cyberattacks are aimed at the public, with many targeted at other hacker groups, often to steal information that others have gathered.

## Bibliography

- Wikimedia Foundation (21 February 2023) [Security hacker](https://en.wikipedia.org/wiki/Security_hacker), Wikipedia, accessed 2 April 2023
- Wikimedia Foundation (1 April 2023) [Black hat (computer security)](https://en.wikipedia.org/wiki/Black_hat_(computer_security)), Wikipedia, accessed 2 April 2023
- Wikimedia Foundation (2 November 2022) [Grey hat](https://en.wikipedia.org/wiki/Grey_hat), Wikipedia, accessed 2 April 2023
- Wikimedia Foundation (27 March 2023) [White hat (computer security)](https://en.wikipedia.org/wiki/White_hat_(computer_security)), Wikipedia, accessed 2 April 2023
- [What are ethical implications of hacking?](https://www.quora.com/What-are-ethical-implications-of-hacking), Quora, accessed 2 April 2023