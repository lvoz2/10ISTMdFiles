# Internet Risks and Mitigation Techniques

<h2 style="display:none" id="top">(top)</h2>

![Hacker attempts to hack into their WIFI network](https://images.idgesg.net/images/article/2019/03/hack-your-own-wi-fi_neon-wi-fi_keyboard_hacker-100791531-large.jpg?auto=webp&quality=85,70)

## Introduction

When traversing the Internet, you will find many risks, and be exposed to a vast array of mitigation techniques, such as passwords, to stop hackers getting into systems to get your information. But at some point in your life, your information will be exposed via a hack, and there is a good chance that some hacker database has at least one of your passwords. But do not fret. Most of the time, hackers will be clueless as to who you are, especially if they only have a password to identify you.

## Risks of the Internet

The Internet can be dangerous. It can also be full of fun. But, thanks to the nasty work of black hat hackers, the Internet is full of websites pretending to be your bank, your government, your favourite social media app, or even your favourite gaming services provider. Hackers can get your information through a host of techniques, such as phishing, which invloves a malicious website looking almost exactly the same as what it is trying to replicate, such as your bank website, to then log what your password is so they can hack into that service and steal your money or identity. Hackers can also scam you into thinking they are someone else in dire need, hoping you are happy and willing to part with a large portion of your money. Hackers may also utlise malware to inadvertently deny you access to a website, while they steal information from its servers. They may also infect your device, where they can scrape it for details, encrypt your files and ask you to pay ransom so you can unlock them, or even both - or worse, do the latter but don't give a key, which is what the most devastating cyberattack in history, <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutNotPetya.json\\\"\"]")'>NotPetya</a>, did in 2017. Although this may seem like way to many fronts to defend, a lot of these will not affect you, given the large amount of others that they can attack instead.

## Mitigation Techniques

The Internet can be scary, but with the advent of encryption, many of the attacks above can be mitigated. But for some, you will simply have to try and ignore the scam messages or phishing emails. Often, the best way to defend against this is to install an antivirus software, which can help, but in the case of novel attacks with novel vulnerabilities, there isn't much that can be done to defend your data. This is why cyberattacks can be so damagaing. Some simple steps that you can do to mitigate the risk of cyber attack are:

1. Use an antivirus software,
2. Use a firewall, which can block malicious data from getting to your device,
3. Don't click on poorly worded emails or messages that you weren't expecting,
4. Always use a password, or, if the service supports it, a passkey
5. Regulaly check the security of your passwords
6. Make sure your passwords are unique, which can be done bby using a password manager, or by using password schemes that are easy to rememeber
7. Before entering sensitive information online, check whether the URL of your site starts with `https://` and not `http://` (Note the `s` at the end of `http` in the first example, which stands for 'Secure')

These techniques can help to reduce the chance of attack enourmously, but, as always, there is always a chance of your information being stolen or for your bank account to become compromised.