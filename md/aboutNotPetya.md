# About the NotPetya cyberattack

<h2 style="display:none" id="top">(top)</h2>

![User Interface when infected by NotPetya, which is also known as 'Golden Eye'](https://www.cnet.com/a/img/resize/d1d794e67ab00f024e88924de9615b8e78308a41/hub/2017/07/07/34977c24-75fc-4a5b-b4c1-13e34158ff66/goldeneye-ransomware-note.jpg?auto=webp&fit=crop&height=675&width=1200)<p style="font-size: 0.8em; line-height: 1.15;">The message that the malware gives to users after device infection. Note that NotPetya is also known as 'Golden Eye', which is a reference to the James Bond movie of the same name.</p>

## Introduction

The NotPetya cyberattack was one of the most costly cyberattacks of all, with the damage estimated to be over US$10 billion. It affected the entire world, even though it was meant only for Ukraine. It started on the 27<sup>th</sup> of June 2017, and is part of a family of encrypting ransomwares called 'Petya'. Although malware in the Petya family can decrypt the files that they encrypt, NotPetya doesn't exhibit this activity, instead it will permanently encrypt files without the chance of decryption. The malware only affects Windows devices, which, given the large market share that the Windows operating system has, increases the potential damage of the cyberattack when compared to affecting only devices runnning macOS or Linux. It is beleived to be a state-sponsored action from Russia, being made by the Sandworm group.

## Modus Operandi

The NotPetya malware was spread using a hijacked version of the updating software of M.E.Doc, which is a Ukrainian tax preparation program that is almost universal in Ukraine. This was able to be done thanks to a backdoor in the updating software, which, according to anaylsis be ESET, had been present for at least 6 weeks before the attack, meaning that it was well planned. This enabled NotPetya to get installed onto computers, where it infects the booting sequence of the device, forcing it to undergoe a restart. Upon restart, the malware encrypts the Master File Table, which is an index of all the files and folders in an NTFS file system. From here, it displays its ransomware message, demanding payment in Bitcoin. To propagate, NotPetya exploits the <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutEternalBlue.json\\\"\"]")'>EternalBlue</a> exploit, similar to <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutWannaCry.json\\\"\"]")'>WannaCry</a>, which enables it to gain a list of passwords for network devices, using them in conjuction with PSExec to run code on other network connected devices, enabling the spread of the ransomware. Although it purports itself as a ransomware, it can be said not to be a ransomware, as the ransom is extremely low, at only US$300, with the same ID for the bitcoin wallet used for each infection. The latter is normally a bad idea, as it enables the tracking of payments and the possiblity of being blocked, which is avoided by conventional ransomware by generating unique IDs for each infection. These two factors, along with the inability to decrypt files, suggest that the malware was instead made to do as much damage in as little time, and had no financial incentive, which may be because of it being a state-sponored action of Russia. This case is furthered by the absence of damage within Russia.

## Reach

The NotPeta ransomware was not limited to Ukraine, as, because it propagates through the networks of a target, it would affect the networks af all businesses with offices in Ukraine, such as, but not limited to, Maesrk, Merck, Rosneft, Reckitt-Benckiser, Beiersdorf, DHL, Mondelez International, FedEx, as well as the Cadbury Chocolate Factory in Hobart, which was the first business in Australia to be affected by a Petya malware. With Maersk, the loss of revenue thanks to NotPetya has been estimated to be in the range of US$200 million - US$300 million, representing at least 2% of the damage of the malware. FedEx lost about US$400 million to the NotPetya malware. On top of these corporate damages, as a result of the cyberattack, the radiation monitoring systems at the Chernobyl Nuclear Power Plant went offline, which posed a risk to the entirety of Europe from a repeat of the 1986 disaster at unit No. 4. The cyberattack was so powerful, that former US Homeland Security adviser Tom Bossert, who at the time of NotPetya unleashing on the world, was the most senior cybersecurity-focused official in the US, said that "\[NotPetya\] was the equivalent of using a nuclear bomb to achieve a small tactical victory". The speed at which the attack propagated allowed for it to take over entire networks within seconds, and, because it accesses other devices using passwords, allowed it to get into devices that had installed the recently released patch for the EternalBlue vulnerability. The speed of the attack was so fast, that it took 45 *seconds* to take down the network of a large Ukrainian Bank.

## Extra Information

### What were the damages to companies because of the attack?

| Company           | Damage (in USD) |
| ----------------- | ---------------:|
| Merck             |    $870,000,000 |
| FedEx             |    $400,000,000 |
| Saint-Gobain      |    $384,000,000 |
| Maersk            |    $300,000,000 |
| Mondelez          |    $188,000,000 |
| Reckitt Benckiser |    $129,000,000 |

The total damages of these six companies is US$2,271,000,000, representing a maximum of 22.71% of all damages.

## Bibliography

- Greenberg A (22 August 2018) [The Untold Story of NotPetya, the Most Devastating Cyberattack in History](https://www.wired.com/story/notpetya-cyberattack-ukraine-russia-code-crashed-the-world/), WIRED, accessed 28 March 2023
- Wikimedia Foundation (24 March 2023) [Petya and NotPetya](https://en.wikipedia.org/wiki/Petya_and_NotPetya), Wikipedia, accessed 18 February 2023

## See Also

- [Wikipedia Article on NotPetya](https://en.wikipedia.org/wiki/Petya_and_NotPetya)
- [WIRED - The Untold Story of NotPetya, the Most Devastating Cyberattack in History](https://www.wired.com/story/notpetya-cyberattack-ukraine-russia-code-crashed-the-world/)