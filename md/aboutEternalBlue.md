# About the EternalBlue exploit

<h2 style="display:none" id="top">(top)</h2>

![Logo for the EternalBlue exploit](https://www.loginradius.com/blog/static/579582de5b60f995b00553dd02616f3c/03979/etbluecover.png)

## Introduction

The EternalBlue exploit ([CVE Record](https://www.cve.org/CVERecord?id=CVE-2017-0144)) was developed by the US National Security Agency (NSA), specifically the Equation Group, and was later leaked by the Shadow Brokers hacker group on the 14<sup>th</sup> of April, 2017. It primarily affects all Windows operating systems from Windows 95 all the way to Windows 10, and from Windows Server 2003 to 2016.The NSA knew about the exploit for at least 5 years prior to the leak, and, when it found out about the stolen information, told Microsoft, which then released a patch for it a month prior to the public leaking of information, but after the inital theft.

## Modus Operandi

EternalBlue operates through the explotation of a vulnerability in Microsoft's implemetation of the Server Messaging Block (SMB) protocol, specifically SMBv1, which allows shared access to files and printers on a network. This vulnerability exists because of SMB mishandling specially-crafted packets of information, allowing hackers to remotely execute code on a device. Specifically, there are three bugs that enable EternalBlue, which are listed below:

1. The difference between definition of two SMB subcommands, SMB_COM_TRANSACTION2 and SMB_COM_NT_TRANSACT. The latter will call for a packet twice the size of the former, and, when timed correctly, a packet can be requested by the client initally with NT_TRANSACT, and then immeadieatly after with TRANSACTION2, causing a validation error. The protocol then assigns type and size of both packets according to the information of the later packet. If the last is smaller, the first will occupy more space than allocated.
2. A miscalculation when trying to cast an OS/2 (This is the os name for the original device with SMB) FileExtended Attribute to an NT (Windows) FileExtended Attribute, causing buffer overflow with sufficently large packets
3. The last bug allows heap spraying, which is a technique that overwrites memory with other code at a set memory address, which can then be pointed to and executed.

This can then be used to load code for malware and execute it with a small initial payload. Although the exploit primarily targets Windows devices, it can also affect any ohter device utlilising Microsoft's implmentation of SMBv1, including Siemens ultrasound equipemnt and many others, potentially allowing other attack vectors outside of Windows.

## Extensivity

Although a patch was released by Microsoft in security bulletin MS17-010, one month before public release, as of May 2019, there were over a million Windows devices that were still vulnerable.
![Heat map of the world, showing the extensivity of devices vulnerable to EternalBlue, as of May 2019](https://www.sentinelone.com/wp-content/uploads/2019/05/3-Shodan-Eternalblue-top-countries.jpg)

## Notable cyberattacks

- <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutNotPetya.json\\\"\"]")'>NotPetya</a> (Damage: at least US$10 billion)
- <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutWannaCry.json\\\"\"]")'>WannaCry</a> (Damage: between US$100 million and US$4 billion)

## Bibliography

- Wikimedia Foundation (10 December 2022) [EternalBlue](https://en.wikipedia.org/wiki/EternalBlue), Wikipedia, accessed 18 February 2023
- Wikimedia Foundation (14 March 2023) [Server Message Block](https://en.wikipedia.org/wiki/Server_Message_Block), Wikipedia, accessed 1 April 2023
- Cronin A (13 April 2012) [Understanding Heap Spraying](https://andyrussellcronin.wordpress.com/2012/04/13/understanding-heap-spraying/), Andy Cronin – Ethical Hacking Honours Project – Drive-by Healing, accessed 1 April 2023
- SentinelOne (27 May 2019) [EternalBlue Exploit: What It Is And How It Works](https://www.sentinelone.com/blog/eternalblue-nsa-developed-exploit-just-wont-die/), SentinelOne Blog, accessed 1 April 2023