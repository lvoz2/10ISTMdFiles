# About the WannaCry cyber attack

<h2 style="display:none" id="top">(top)</h2>

![Screenshot of the ransom note left on an infected system](https://media.kasperskydaily.com/wp-content/uploads/sites/92/2022/08/23211037/wannacry-hsitory-lessons-ransom-note.jpg)

## Introduction

The WannaCry ransomware was a worm that utlilised the <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutEternalBlue.json\\\"\"]")'>EternalBlue</a> exploit. It started at 7:44 UTC on the 12<sup>th</sup> of May 2017, and was halted at 15:03 UTC by way of the registration of a killswitch, found by Marcus Hutchins. The attack was estimated to have affected 300,000 devices in as many as 150 countries. The damages associated with the attack have been estimated to be in the range of hundreds of millions of dollars to billions (US). The attack, occording to the US & UK, originated in North Korea, being created by the Lazarus Group. When the worm hits a device, it encrypted the device's files and displays a ransom message, which asks for US$300 in bitcoin, doubling this if the ransom is not paid within three days. 

## Modus Operandi

To infect a device, the worm uses the <a href="#" onclick='addCmdLine("[\"render /f \\\"aboutEternalBlue.json\\\"\"]")'>EternalBlue</a> exploit to gain access to the device, after which it checks whether the killswitch domain name, which is `iuqerfsodp9ifjaposdfjhgosurijfaewrwergwea.com`, has been registered. If not, the infection proceeds by using a modified version of the DoublePulsar backdoor creation tool, which enables WannaCry to encrypt all the files on the device and display the ransom message along with the bitcoin wallet key. While that happens, the worm searches the network for other devices, where it will repeat the cycle of infection. 

## Reach

WannaCry was, at the time, unprecedented in scale, according to Europol, with estimates saying that around 200,000 devices were infected across 150 countries. Russia, Ukraine, India and Taiwan were the most affected, according to a report by Kaspersky Lab. The attack infected up to 70,000 medical devices in hospitals run by the NHS throughout England and Scotland, with some hospitals having to turn away non-critical patients in the wake of the attack. After the attack had subsided, it was reported that 327 ransoms had been paid, with a total of US$130,634.77 being transferred to the hackers.

## Bibliography

- Wikimedia Foundation (21 March 2023) [WannaCry ransomware attack](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack), Wikipedia, accessed 18 February 2023
- Kaspersky Lab (23 August 2022) [The chronicle of WannaCry](https://www.kaspersky.com/blog/wannacry-history-lessons/45234/), kaspersky daily, accessed 1 April 2023