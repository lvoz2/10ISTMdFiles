# Influences of role in Computer Security

<h2 style="display:none" id="top">(top)</h2>

![Image of a stream of data, getting encrypted as it passes through a padlock](https://www.kaspersky.com/content/en-global/images/repository/isc/2021/encryption-1.jpg)

## Introduction

Different roles have different influeneces on cybersecurity. Some roles are dedicated to the topic, others will simply implement encryption algoritms to protect your data. Mathematicians create encryption algorithms, which are used to scramble information and make it unreadable to anyone looking at it, except for the intended recipient. Then, programmers will implement the algorithm in their scripts. But sometimes, encryption is not enough to secure data - and so we have to create defences against all kinds of attacks.

## Cyber security experts

Cyber security experts, such as white hat hackers, help to secure your information by finding and fixing vulnerabilities in code and systems. They also analyse past cyberattacks, to glean as much information as possible about how the attack worked and what vulnerabiliites they exploited. Often, they will reveerse engineer malware to see what it exploits. They also validate whether an encryption algorithm is still relevant, or if it has been cracked by hackers.

## Mathematicians & Programmers

These two roles influence cyber security because they, when working together, can help to create the encryption algorithms used to secure data, such as RSA, AES, and SHA. These three encryption algorithms are the most commonly used ones, with RSA underpinning the entirety of the Internet. Mathematicians create and define how encryption standards work, before passing that information on to programmers, who can then create libraries and scripts that implement the algorithm. Once this is done, it is put online for all to use.

## Programmers

On their own, programmers are also influential, as they create the software that is then used by others, and that also has accidental bugs and vulnerabilities encoded in it. Although this is bad, this is also inevitable, and when a patch is made, the patch may also add another bug or vulnerability to the code base. This is the source of most vulnerabilities, but not all. It is also impossible to fully protect code from attack. For example, this webpage has no filter on the output of the markdownn source of these articles. This allows malicious actors to insert JavaScipt code that registers keystrokes and clicks, among other things, and then send it to their servers for processing. The only securing mechanism against this is the hope that no one can alter the local copies of this, as well as the source on GitLab. These are my only security mechanisms against what is called `XSS`, or `Cross-Site-Scripting` vulnerabilities.

## Bibliography

- [*What is Data Encryption?*](https://www.kaspersky.com/resource-center/definitions/encryption) (n.d.), kaspersky, accessed 2 April 2023